import connexion
import six

from openapi_server.models.quote_item import QuoteItem  # noqa: E501
from openapi_server import util


def get_random_quote(quote_tag=None):  # noqa: E501
    """Randomly get a quote

    Just send a request to get a ramdom quote  # noqa: E501

    :param quote_tag: pass an optional tag to select quote subject
    :type quote_tag: str

    :rtype: List[QuoteItem]
    """
    return 'do some magic!'
