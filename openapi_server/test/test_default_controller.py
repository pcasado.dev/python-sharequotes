# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
from six import BytesIO

from openapi_server.models.quote_item import QuoteItem  # noqa: E501
from openapi_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_get_random_quote(self):
        """Test case for get_random_quote

        Randomly get a quote
        """
        query_string = [('quoteTag', 'quote_tag_example')]
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/pcasado.dev/sharequotes/1.0.0/quote',
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    unittest.main()
